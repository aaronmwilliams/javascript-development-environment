# javascript-development-environment

## Purpose of Project
Boilerplate / starter kit for a Javascript project.

## Installing & Running
To download all dependencies run `npm install`

`npm start` - runs security checks and starts local development server

`npm run open:src` - just starts local development server

`npm run security-check` - just runs the security checks

`npm run localtunnel` - just starts sharing environment

`run share` - runs local dev environment and sharing environment

The local server runs on http://localhost:3000

To run any package scripts by their direct commands instead of the custom npm scripts please see sections below.

## Mock API
When the app is started under localhost a `JSON Server` is started and test data is created using `json-schema-faker`.

The stub API is ran on http://localhost:3001/.

## Continuous Integration
Jenkinsfile is configurered to run `npm test`

## Project Automation
Project uses NPM Script. Please see the scripts listed above.

Other project automation projects considered are listed below:

**Grunt:** configuration over code.

**Gulp:** faster than grunt as it's inmemory

## Linting
ESLint default setting are used on this project.

To run a ad-hoc linting check use command `npm run lint`

To watch over the files for changes run `npm run lint:watch`

## Transpiling
This project uses Babel to transpile the latest JS code. configuration is setup in `.babelrc`

Also considered for transpiling was **TypeScript:** suserset of JS with autocompletion, refactoring.

## Bundling
Webpack is used to bundle the files for browser support.

See `webpack.config.dev.js` for the webpack configuration.

To view unbundled code in the browser add `debugger` in the files and visit the page on a browser console.

Other bundling tools which were considered are:

**Browserify:** simple and oldest bundle tool

**Rollup:** new and features tree-shaking and fast performance

**JSPM:** features a runtime loader and good package manager

## Editor Config
Refer to this file `.editorconfig` for the code styling standards on this project.

Please ensure you have the Editor Config plugin for your IDE.

## Node Security Platform
If you have NSP installed you can run `nsp check` to scan the project for security vulnerabilities

## Development Web Server
This project is using Express as a local development server. To run this server directly use this command `node buildScripts/srcServer.js`

The setup for this server is in `/buildScripts/srcServer.js`

Other alternatives which were considered are listed below:

**http-server:** simple & lightweight. Live-reloading

**koi hapi:** more featured and can also be used on production. Better for node projects

**Browsersync:** Can sync multiple browsers when testing the web pages

**dudo:** Works with Browserify bundler

**webpack:** Dev server within webpack

## Sharing your development work
If you have localtunnel installed you can run `lt --port 3000 --subdomain [YOUR CHOSEN SUBDOMAIN]` and your work can be shared to others via a URL.

Other alternatives which were considered are listed below:

**ngrok:** a bit more setup and secure

**now:** uses their own servers

**Surge:** users their own servers. only works with static files